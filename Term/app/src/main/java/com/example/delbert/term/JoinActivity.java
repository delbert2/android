package com.example.delbert.term;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Delbert on 2016-05-14.
 */
public class JoinActivity extends Activity {
    EditText id, pw, pwCheck, phone;
    Button btnJoin, btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        id = (EditText) findViewById(R.id.et_id);
        pw = (EditText) findViewById(R.id.et_pw);
        pwCheck = (EditText) findViewById(R.id.et_pwc);
        phone = (EditText) findViewById(R.id.et_phone);

        btnJoin = (Button) findViewById(R.id.bt_join_01);
        btnExit = (Button) findViewById(R.id.bt_exit);

        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String idString = id.getText().toString();
                String pwString = pw.getText().toString();
                String pwCheckString = pwCheck.getText().toString();
                String phoneString = phone.getText().toString();

                int check = emptyCheck(idString, pwString, pwCheckString, phoneString);
                switch (check) {
                    case 0:
                        boolean valid = pwValidation(pwString, pwCheckString);
                        if (valid) {
                            Intent toMain = new Intent(JoinActivity.this, MainActivity.class);
                            startActivity(toMain);
                        } else {
                            Toast.makeText(JoinActivity.this, "Password를 확인해주세요", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        Toast.makeText(JoinActivity.this, "id를 입력해주세요", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(JoinActivity.this, "Password를 입력해주세요", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(JoinActivity.this, "Password를 입력해주세요", Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        Toast.makeText(JoinActivity.this, "연락처를 입력해주세요", Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toMain = new Intent(JoinActivity.this, MainActivity.class);
                startActivity(toMain);
            }
        });
    }

    private boolean pwValidation(String pw, String pwCheck) {
        if (pw.equals(pwCheck)) {
            return true;
        } else {
            return false;
        }
    }

    private int emptyCheck(String id, String pw, String pwCheck, String phone) {
        if (id == null || id.length() == 0) {
            return 1;
        } else if (pw == null || pw.length() == 0) {
            return 2;
        } else if (pwCheck == null || pwCheck.length() == 0) {
            return 3;
        } else if (phone == null || phone.length() == 0) {
            return 4;
        } else {
            return 0;
        }

    }
}


