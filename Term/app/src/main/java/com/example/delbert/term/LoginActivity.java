package com.example.delbert.term;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Delbert on 2016-05-14.
 */
public class LoginActivity extends Activity {
    Button loginBtn;
    EditText id, pw;
    CheckBox auto;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginBtn = (Button) findViewById(R.id.bt_login_01);
        id = (EditText) findViewById(R.id.et_id_01);
        pw = (EditText) findViewById(R.id.et_pw_01);
        auto = (CheckBox) findViewById(R.id.cb_autologin);
        pref = getSharedPreferences("autoLogin", MODE_PRIVATE);
        editor = pref.edit();

        if (pref.getBoolean("auto", false)) {
            id.setText(pref.getString("id", ""));
            pw.setText(pref.getString("pw", ""));
            auto.setChecked(true);
        }
        auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String inputId = id.getText().toString();
                    String inputPw = pw.getText().toString();
                    editor.putString("id", inputId);
                    editor.putString("pw", inputPw);
                    editor.putBoolean("auto", true);
                    editor.commit();
                } else {
                    editor.clear();
                    editor.commit();
                }
            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toSearch = new Intent(LoginActivity.this, SearchActivity.class);
                startActivity(toSearch);
            }
        });

    }
}



