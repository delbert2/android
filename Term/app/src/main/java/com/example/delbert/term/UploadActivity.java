package com.example.delbert.term;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Delbert on 2016-05-14.
 */
public class UploadActivity extends Activity {
    RatingBar rating;
    TextView tv;
    Button cancel, enroll;
    EditText bookname, writer, publisher, assess;
    Uri mImageCaptureUri;
    ImageView mPhotoImageView;
    Button mButton;

    information memberinformation;

    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_GALLERY = 1;
    private static final int CROP_FROM_CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        memberinformation = new information(this);
        enroll = (Button) findViewById(R.id.bt_enroll);
        tv = (TextView) findViewById(R.id.tv_assess);
        bookname = (EditText) findViewById(R.id.et_bookname);
        writer = (EditText) findViewById(R.id.et_writer);
        publisher = (EditText) findViewById(R.id.et_publisher);
        assess = (EditText) findViewById(R.id.et_assess);
        rating = (RatingBar) findViewById(R.id.rb_assess);
        mButton = (Button) findViewById(R.id.bt_upload);
        mPhotoImageView = (ImageView) findViewById(R.id.iv_book);
        cancel = (Button)findViewById(R.id.bt_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toSearch = new Intent(UploadActivity.this, SearchActivity.class);
                startActivity(toSearch);
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener cameraListner = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doTakePhotoAction();
                    }
                };
                DialogInterface.OnClickListener galleryListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doTakeGalleryAction();
                    }
                };
                DialogInterface.OnClickListener cancelListner = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                new AlertDialog.Builder(UploadActivity.this)
                        .setTitle("사진 올리기")
                        .setPositiveButton("사진촬영", cameraListner)
                        .setNeutralButton("앨범선택", galleryListener)
                        .setNegativeButton("취소", cancelListner)
                        .show();
            }
        });
        enroll.setOnClickListener(new View.OnClickListener() {
            SQLiteDatabase db;
            ContentValues values;

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(UploadActivity.this)
                        .setTitle("등록하시겠습니까?")
                        .setPositiveButton("예", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try{
                                    String booknameString = bookname.getText().toString();
                                    String writerString = writer.getText().toString();
                                    String publisherString = publisher.getText().toString();
                                    String assessString = assess.getText().toString();
                                    db = memberinformation.getWritableDatabase();
                                    values = new ContentValues();
                                    values.put("bookname", booknameString);
                                    values.put("writer", writerString);
                                    values.put("publisher", publisherString);
                                    values.put("assess", assessString);
                                    db.insert("member", null, values);
                                    memberinformation.close();
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                                Toast.makeText(UploadActivity.this, "등록되었습니다.", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(UploadActivity.this, "취소되었습니다.", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                tv.setText("평점 : " + rating);
            }
        });
    }


    private void doTakePhotoAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    private void doTakeGalleryAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case CROP_FROM_CAMERA: {
                final Bundle extras = data.getExtras();

                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");
                    mPhotoImageView.setImageBitmap(photo);
                }

                File f = new File(mImageCaptureUri.getPath());
                if (f.exists()) {
                    f.delete();
                }
                break;
            }
            case PICK_FROM_GALLERY: {
                mImageCaptureUri = data.getData();
            }
            case PICK_FROM_CAMERA: {
                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");

                intent.putExtra("outputX", 200);
                intent.putExtra("outputY", 200);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);
                startActivityForResult(intent, CROP_FROM_CAMERA);
                break;
            }
        }
    }

}
