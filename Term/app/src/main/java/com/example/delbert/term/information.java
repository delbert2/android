package com.example.delbert.term;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Delbert on 2016-05-16.
 */
public class information extends SQLiteOpenHelper{
    final static String TAG = "information";
    public information(Context c){
        super(c, "memberinformation.db",null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG,"onCreate()");
        String query = "CREATE TABLE member (_id INTEGER PRIMARY KEY AUTOINCREMENT,bookname TEXT, writer TEXT, publisher TEXT, assess TEXT);";
        db.execSQL(query);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG,"onUpgrade()");
        String query = "DROP TABLE IF EXISTS member";
        db.execSQL(query);
        onCreate(db );

    }
}
