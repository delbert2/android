package com.example.delbert.term;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

/**
 * Created by Delbert on 2016-05-14.
 */
public class SearchActivity extends Activity {
    Button uploadBtn;
    Spinner spin;
    ArrayAdapter adapt;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        uploadBtn = (Button) findViewById(R.id.bt_upload_01);
        spin = (Spinner)findViewById(R.id.sp_01);
        adapt = ArrayAdapter.createFromResource(this,R.array.search,android.R.layout.simple_spinner_item);
        spin.setAdapter(adapt);

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toUpload = new Intent(SearchActivity.this, UploadActivity.class);
                startActivity(toUpload);
            }
        });

    }


}
